#Git Deployd

Git Deployd automatically updates git repo on server when changes are pushed to bitbucket or github.
It uses POST hooks, cron job sh and PHP scripts to write trigger file which will apply defined git commands on repository.

##How it works

- When you push changes to github or bitbucket data is sent to your defined url *http://host/gitdeployd/deploy*
- PHP script on that url is called and creates file indicating that update is needed
- If script from cron job founds that update file exists, it applies predefined git or other command on git repository
- Access to scripts are logged in files - *deploy.log* (deployment details) and *badass.log* (unauthorized access)

##Installation

- Copy **git-deployd** folder to your server
- Make  **gitdeployd/deploy** writable by web process
- Adjust path settings in **gitdeployd/sh/deployd.sh**
- Add your ip to allowed ip list in **gitdeployd/deploy/index.php** to access script manually and trigger git update
- Setup github (**Repo > Settings > Service Hooks**) or bitbucket (**Repo > Admin > Services**) hook

##What's next?

- I'm planing to add ftp or ssh support to upload files to web server,  in case GIT is unavailable on your web hosting
- Parse data received on post and run command only if conditions are met. For ex. predefined git command will be applied only if updates are on branch MASTER.

##Credits

Idea broth from [theme.fm](http://theme.fm/2011/07/github-playground-servers-auto-pull-from-master-1113) and [oodavid](https://gist.github.com/1809044)
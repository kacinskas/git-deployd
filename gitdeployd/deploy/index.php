<?php
$version = '0.1.5';
$allowedIp = array(
   /* '127.0.0.1', *//* custom ip for manual access */
    '63.246.22.222', /* bitbucket */
    '207.97.227.253', '50.57.128.197', '108.171.174.178', /* github */
);

/*
 * Check if ip is allowed if not write time and ip address to log
 * @TODO create more acurate ip detection - use other params not only REMOTE_ADDR
 */
if (!isset($_SERVER['REMOTE_ADDR']) || !in_array($_SERVER['REMOTE_ADDR'], $allowedIp)) {

    $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'none';

    // path and name 
    $target_file = "badass.log";

    // string data that is to be placed into the file
    $time = date('Y-m-d[H:i:s]');
    $target_file_data = "$time IP:$ip" . PHP_EOL;

    // opening and appending to the file 
    $handle = fopen($target_file, "a");
    fwrite($handle, $target_file_data); // write it 
    fclose($handle);

    echo 'bad origin, closing...';
    exit();
}

/*
 * Write file for trigger cronjob
 */
$repo = 'smartsecurity';
$filePath = dirname(__FILE__) . '/' . 'start';
$file = fopen($filePath, 'w+');
$status= $file === false ? 'Unable to write git update trigger file' : "Cron task for git update trigger writen.";
fclose($file);

/*
 * Write file for trigger cronjob
 */
// The commands
$commands = array(
    'echo $PWD',
    'whoami',
    'git branch',
    'git status',
);

// Add status message
$output = PHP_EOL.$status.PHP_EOL;
// Run the commands for output
foreach ($commands AS $command) {
    // Run it
    $tmp = shell_exec($command . '  2>&1');
    // Output
    $output .= "<span style=\"color: #222;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
    $output .= htmlentities(trim($tmp)) . "\n";
}


// Make it pretty for manual user access (and why not?)
?>
<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Git Deployd</title>
    </head>
    <body style="background-color: #CECECE; color: #404040; font-weight: bold; padding: 0 10px;">
        <pre>
 .  ____  .    ____________________________
 |/      \|   |                            |
[| <span style="color: #FF0000;">&hearts;   &hearts;</span> |] /          Git Deployd v<?php echo $version; ?> |
 |___==___|  \         © kacinskas.eu 2012 |
              |____________________________|

            <?php echo $output; ?>
            
        </pre>
    </body>
</html>
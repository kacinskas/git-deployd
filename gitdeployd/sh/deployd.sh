#!/bin/bash

#
# Script checks if trigger file 'start' exists
# And applies git update command
#
# Add this script to cron job, eg.:
# * * * * * /home/user/www/gitdeployd/sh/deployd.sh 2>&1 >> /home/user/www/gitdeployd/deploy/deploy.log
#
# ">> /home/eldes-ssl/gitdeployd/deploy/deploy.log" this will also writes script output to log file (put your own path here)
#


# git command to execute
GITCOMMAND="git fetch git@bitbucket.org:kacinskas/git-deployd.git master && git reset --hard FETCH_HEAD"

# run git command here
GITREPOPATH="/home/user/www"

# path to the Git Deployd main folder
DEPLOYDPATH="/home/user/www/gitdeployd/"

# path where git update trigger file 'start' is written and monitored and where logs are created
LOGPATH=$DEPLOYDPATH"deploy/"


# Time tracking
FSTART=$(date +%Y-%m-%d[%T])
START=$(date +%s)

# look for start trigger
if [ -f $LOGPATH"start" ]
then
	# trigger file found so remove it and apply command
	# write to log, START
	cd $LOGPATH && echo "$FSTART deploy started.." >> deploy.log

	# remove file
	rm -f $LOGPATH"start"
	# apply command
	cd $GITREPOPATH && eval $GITCOMMAND

	# time tracking
	END=$(date +%s)
	FEND=$(date +%Y-%m-%d[%T])
	DIFF=$(( $END - $START ))

	# write to log, END
	cd $LOGPATH && echo "$FEND deploy total time $DIFF seconds" >> deploy.log

else
	# trigger file not found write to log
	cd $LOGPATH && echo "$FSTART no git fetch start script found" >> deploy.log

fi
